eg25-manager (0.4.3-1) unstable; urgency=medium

  * New upstream version 0.4.3
  * d/control: build-depend on `scdoc`
    This is required for building the manpages. Also bump Standards-Version,
    no other changes needed.
  * d/watch: fix watch file

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Feb 2022 15:20:14 +0100

eg25-manager (0.4.2-1) unstable; urgency=medium

  * d/gbp.conf: update for current Mobian workflow
  * New upstream version 0.4.2

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Wed, 08 Dec 2021 18:24:38 +0100

eg25-manager (0.4.1-1) unstable; urgency=medium

  * New upstream version 0.4.1
  * debian: drop distro-specific systemd service.
  * d/copyright: add missing entries

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Fri, 08 Oct 2021 11:02:27 +0200

eg25-manager (0.4.0-1) unstable; urgency=medium

  [ undef ]
  * d/service: Use systemd to sandbox eg25-manager.
  * d/salsa-ci: Add Mobian's CI

  [ Arnaud Ferraris ]
  * New upstream version 0.4.0
  * d/eg25-manager.service: be less restrictive.
  * d/control: add libcurl as build dependency

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 01 Sep 2021 00:44:04 +0200

eg25-manager (0.3.0-1) unstable; urgency=medium

  * New upstream version 0.3.0

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Fri, 28 May 2021 13:58:33 +0200

eg25-manager (0.2.1-1) unstable; urgency=medium

  * New upstream version 0.2.1

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 21 Feb 2021 16:41:31 +0100

eg25-manager (0.2.0-1) unstable; urgency=medium

  * New upstream version 0.2.0
  * d/gbp.conf: enable multimaint-merge
  * d/eg25-manager.service: remove deprecated -g option

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 20 Feb 2021 22:26:19 +0100

eg25-manager (0.1.2-1) unstable; urgency=medium

  * New upstream release 0.1.2
  * d/eg25-manager.service: enable GNSS management

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 14 Jan 2021 00:09:23 +0100

eg25-manager (0.1.1-1) unstable; urgency=medium

  * d/control: build-depend on gudev
  * New upstream release 0.1.1

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 18 Dec 2020 01:42:06 +0100

eg25-manager (0.1.0-1) unstable; urgency=medium

  * New upstream release 0.1.0

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 14 Dec 2020 16:45:20 +0100

eg25-manager (0.0.6-1) unstable; urgency=medium

  * New upstream release 0.0.6
  * d/patches: drop upstreamed patches

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 11 Dec 2020 15:11:26 +0100

eg25-manager (0.0.5-2) unstable; urgency=medium

  * d/patches: fix crash on modem recovery

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 11 Dec 2020 14:33:14 +0100

eg25-manager (0.0.5-1) unstable; urgency=medium

  * New upstream release 0.0.5

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 11 Dec 2020 13:38:41 +0100

eg25-manager (0.0.4-1) unstable; urgency=medium

  * d/control: build only on arm64.
  * d/service: restart daemon on failure
  * d/control: build-depend on libusb-1.0

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 11 Dec 2020 12:51:41 +0100

eg25-manager (0.0.3-1) unstable; urgency=medium

  * New upstream release 0.0.3

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 10 Dec 2020 21:34:26 +0100

eg25-manager (0.0.2-1) unstable; urgency=medium

  * New upstream release 0.0.2

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 10 Dec 2020 19:50:50 +0100

eg25-manager (0.0.1-1) unstable; urgency=medium

  * Initial Debian packaging

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 10 Dec 2020 15:19:15 +0100
